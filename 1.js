const tabsNav = document.querySelector(".tabs-nav");
const tabsItems = document.querySelectorAll(".tabs_item");

tabsNav.addEventListener("click", onTabClick);

function onTabClick(event) {
    const currentBtn = event.target;
    if (!currentBtn.classList.contains("tabs_nav-btn")) return;

    const tabId = currentBtn.getAttribute("data-tab");
    const currentTab = document.querySelector(tabId);

    if (!currentBtn.classList.contains("active")) {
        Array.from(document.querySelectorAll(".tabs_nav-btn, .tabs_item")).forEach(function (item) {
            item.classList.remove("active");
        });

        currentBtn.classList.add("active");
        currentTab.classList.add("active");
    }
}